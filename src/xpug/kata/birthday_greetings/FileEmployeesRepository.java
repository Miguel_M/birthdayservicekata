package xpug.kata.birthday_greetings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class FileEmployeesRepository implements EmployeesRepository  {
	private String fileName;

	public FileEmployeesRepository(String fileName) {
		this.fileName = fileName;
		
	}
	public List<Employee> findEmployeesWhoseBirthdayIs(OurDate today) throws FileNotFoundException, IOException,
			ParseException {
		   
		List<Employee> employeesWithBirthdayToday = new ArrayList<Employee>();
	    System.out.println("Abriendo archivo");
		BufferedReader in = new BufferedReader(new FileReader(this.fileName));		String str = "";
		str = in.readLine(); // skip header
		System.out.println("Primera linea de archivo");
		while ((str = in.readLine()) != null) {
		  	String[] employeeData = str.split(", ");
			Employee employee = new Employee(employeeData[1], employeeData[0],
			employeeData[2], employeeData[3]);
			  if (employee.isBirthday(today)) {		
				  employeesWithBirthdayToday.add(employee);
				}
			}
		 in.close();
		 return employeesWithBirthdayToday;
	
		
	}

}
