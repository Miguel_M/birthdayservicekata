package xpug.kata.birthday_greetings;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class BirthdayService {
	int numberOfGreetingsSent;


	public void sendGreetings(EmployeesRepository employeesRepository,EmailService mail) throws IOException, ParseException,
			AddressException, MessagingException {

		OurDate ourDate= new OurDate("2008/10/08");

		List<Employee> employeesWithBirthdayToday = employeesRepository.findEmployeesWhoseBirthdayIs(ourDate);

		for (Employee employee : employeesWithBirthdayToday) {
			mail.sendMessage("sender@here.com",employee);
		}
	}


	public static void main(String[] args) throws AddressException, IOException, ParseException, MessagingException {
		EmployeesRepository repository = new FileEmployeesRepository("employee_data.txt");
		EmailService mail = new SMTPMailService("localhost", 25); 
		BirthdayService service = new BirthdayService();
		service.sendGreetings(repository, mail);
	}

	public int quantityOfGreetingsSent() {
		return numberOfGreetingsSent;
	}
}
