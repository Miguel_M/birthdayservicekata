package xpug.kata.birthday_greetings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface EmployeesRepository {
	public List<Employee> findEmployeesWhoseBirthdayIs(OurDate today) throws FileNotFoundException, IOException, ParseException;

}
